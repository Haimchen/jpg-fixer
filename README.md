# JPG Fixer

Just a tiny CLI tool that helps you rename you jpg files, especially ones imported from an iPhone with .JPG extension.

## Usage

### Linux

If you are on Linux, you can download the compiled executable:

Go to [Downloads]() and download the provided file.

Locate the file and make it executable:
```
chmod +x jpg-fixer
```


### OSX

Unfortunately you need to compile the executable yourself until I have figured out how to cross-compile to OSX on the CI system.

*TODO* Prerequisites

Clone the repo.

```
cargo build --release
```

## Usage

To run it from the directory where the executable is located:

```
./jpg-fixer <path>
```

This will check the provided path. If it is a directory, it will go through all files with extension .jpg or .JPG and rename them like `img_1.jpg`.

If you want a custom prefix instead of "img", you can also use the -prefix parameter:

```
./jpg-fixer <path> -p <prefix>
```

### Installation

????

## Development

To run the programm locally:

```
cargo run <path>
``` 


## TODO

- add option to not rename the files and just change the extension to .jpg
- check in OSX executable
- add info on how to "install" the tool


### Possible Improvements:

- CI builds executable for OSX on release (https://www.reddit.com/r/rust/comments/6rxoty/tutorial_cross_compiling_from_linux_for_osx/)
- add tests

