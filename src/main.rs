use std::ffi::OsStr;
use std::fs;
use std::path::Path;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Cli {
    /// The path to the folder to check for jpg files
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
    /// The prefix to use for renamed image files
    #[structopt(short, long, default_value = "img")]
    prefix: String,
    /// test flag to just analyze the action, but not execute any renaming
    #[structopt(short, long)]
    test: bool,
}

fn is_jpg(path: &Path) -> bool {
    let extension = path
        .extension()
        .unwrap()
        .to_str()
        .unwrap()
        .to_ascii_lowercase();
    let jpg_extension = "jpg";
    return extension == jpg_extension;
}

fn count_digits(count: usize) -> usize {
    return count.to_string().len();
}

fn main() {
    println!("Welcome to the JPG Fixer");
    let args = Cli::from_args();
    let mut is_test = false;
    if args.test {
        println!("Running in test mode - no files will be touched");
        is_test = true;
    }
    // println!("prefix: {}", args.prefix);

    // check if path points to a directory
    // TODO: handle error case properly without panic
    let metadata = fs::metadata(&args.path).unwrap();
    let file_type = metadata.file_type();
    if !file_type.is_dir() {
        println!("Path does not point to a directory. Please provide a valid path.");
        return;
    }
    // First loop: find all jpg files and store them
    let mut jpg_file_paths: Vec<std::path::PathBuf> = Vec::new();
    for (_, entry) in fs::read_dir(args.path).unwrap().enumerate() {
        let entry = entry.unwrap();

        let path = entry.path();
        if !is_jpg(&path) {
            continue;
        };
        jpg_file_paths.push(path);
    }

    // if we are running in test mode, we only print the number of files found
    // otherwise we rename all file paths
    if (is_test) {
        print!(
            "Ran in test mode, would have renamed {0} files.\n",
            jpg_file_paths.len()
        );
    } else {
        for (idx, path) in jpg_file_paths.iter().enumerate() {
            let max_digits = count_digits(jpg_file_paths.len());
            let mut new_path = path.clone();

            // TODO: count files first, then make numbers the right amount of characters with extra zeros
            new_path.set_file_name(format!(
                "{}_{:0width$}",
                args.prefix,
                idx + 1,
                width = max_digits
            ));
            new_path.set_extension("jpg");
            fs::rename(path, new_path);
        }
    }
}
